# crm4dummies
Collision risk modeling for typical occurrences

## Getting started
Clone the repo\
Create the anaconda environment with the follwing:
```conda env create -f env/crm4dummies.yml```\
Activate the environment:
```conda activate crm4dummies```\
 Enjoy!

## License
Open Source MIT license
