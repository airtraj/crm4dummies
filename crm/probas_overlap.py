import multiprocessing as mp
import os
from math import cosh, exp, sinh

import numpy as np
from scipy.stats import laplace, ncx2, norm


def proba_h_olap_norm_norm_nse_circle(
    delta: float, NSE1: float, NSE2: float, ap_radius: float
) -> float:
    """Compute proba of horiz olap using normal distrib and cirle shape.

    Compute the probability for 2 airplanes separated by a distance delta and
    normally distributed NSE of respectively NSE1 and NSE2 to have a vertical
    overlap. The model assumes that airplanes are reprsented by circles of
    similar radius ap_radius.

    Parameters
    ----------
    delta : float
        Reported distance between the 2 airplanes in meters.
    NSE1:
        Navigation System Error for airplane 1.
    NSE2:
        Navigation System Error for airplane 2.
    ap_radius:
        Airplane 1 and 2 radius.

    Returns
    -------
    float
        Probability of horizontal overlap

    Notes
    -----
    Units should be similar for every parameters.
    """
    sigma1 = NSE1 / 1.96
    sigma2 = NSE2 / 1.96
    s2 = sigma1 ** 2 + sigma2 ** 2
    k = 2
    nc = delta ** 2 / s2
    dlim_conv = (ap_radius ** 2) / s2
    return ncx2.cdf(dlim_conv, df=k, nc=nc)


def proba_normal_normal_circle_mc(n_samp, distance, NSE1, NSE2, lambda_xy):
    np.random.seed((os.getpid()))

    n_samp = int(n_samp)
    mu_x1, mu_y1 = 0, 0
    mu_x2, mu_y2 = distance, 0

    variance_x1 = NSE1 / 1.96
    variance_y1 = NSE1 / 1.96
    variance_x2 = NSE2 / 1.96
    variance_y2 = NSE2 / 1.96

    x1, y1 = norm.rvs(mu_x1, variance_x1, n_samp), norm.rvs(
        mu_y1, variance_y1, n_samp
    )
    x2, y2 = norm.rvs(mu_x2, variance_x2, n_samp), norm.rvs(
        mu_y2, variance_y2, n_samp
    )

    dists_squared = (x1 - x2) ** 2 + (y1 - y2) ** 2
    p = np.sum(dists_squared < lambda_xy ** 2) / n_samp
    return p


def proba_normal_normal_circle_mc_para(
    n_samp, distance, NSE1, NSE2, lambda_xy
):
    num_processes = 50
    n_samp = int(n_samp)
    chunks = [
        (n_samp // num_processes, distance, NSE1, NSE2, lambda_xy)
        for _ in range(num_processes)
    ]
    with mp.Pool(num_processes) as pool:
        scores = pool.starmap(proba_normal_normal_circle_mc, chunks)
    return np.mean(scores)


def proba_h_olap_norm_norm_nse_square(
    delta: float, NSE1: float, NSE2: float, lambda_xy: float
) -> float:
    """Compute proba of horiz olap using normal distrib and square shape.

    Compute the probability for 2 airplanes separated by a distance delta and
    normally distributed NSE of respectively NSE1 and NSE2 to have a vertical
    overlap. The model assumes that airplanes are reprsented by square of
    length lambda_xy.

    Parameters
    ----------
    delta : float
        Reported distance between the 2 airplanes in meters.
    NSE1:
        Navigation System Error for airplane 1.
    NSE2:
        Navigation System Error for airplane 2.
    lambda_xy:
        Airplane 1 and 2 length.

    Returns
    -------
    float
        Probability of horizontal overlap

    Notes
    -----
    Units should be similar for every parameters.
    """
    sigma1 = NSE1 / 1.96
    sigma2 = NSE2 / 1.96
    sigma = np.sqrt(sigma1 ** 2 + sigma2 ** 2)
    olap_y = norm.cdf(+lambda_xy, 0, sigma) - norm.cdf(-lambda_xy, 0, sigma)
    olap_x = norm.cdf(lambda_xy, delta, sigma) - norm.cdf(
        -lambda_xy, delta, sigma
    )
    return olap_x * olap_y


def proba_normal_normal_square_mc(n_samp, distance, NSE1, NSE2, lambda_xy):
    np.random.seed((os.getpid()))
    n_samp = int(n_samp)
    mu_x1, mu_y1 = 0, 0
    mu_x2, mu_y2 = distance, 0

    variance_x1 = NSE1 / 1.96
    variance_y1 = NSE1 / 1.96
    variance_x2 = NSE2 / 1.96
    variance_y2 = NSE2 / 1.96

    x1, y1 = norm.rvs(mu_x1, variance_x1, n_samp), norm.rvs(
        mu_y1, variance_y1, n_samp
    )
    x2, y2 = norm.rvs(mu_x2, variance_x2, n_samp), norm.rvs(
        mu_y2, variance_y2, n_samp
    )

    p = (
        np.sum((np.abs(x1 - x2) < lambda_xy) & (np.abs(y1 - y2) < lambda_xy))
        / n_samp
    )
    return p
